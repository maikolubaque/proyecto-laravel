<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//use App\Models\Image;

Route::get('/home', function () {
  /*  
    $images = Image::all();

    foreach ($images as $image){
        echo $image->path;
        echo "by: ".$image->user->name."</br>";

        foreach($image->comments as $comment){
            echo "<strong> cometario: ".$comment->user->name." dijo: ". $comment->content."</strong></br>";
        }

        echo "foto: ".$image->description."</br>";
        echo "likes: ".count($image->likes);
        echo "<hr>";
    }

    die();
*/
    return view('welcome');
});

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/configuracion',  [App\Http\Controllers\UserController::class, 'config'])->name('config')->middleware("auth");
Route::post('/user/update',  [App\Http\Controllers\UserController::class, 'update'])->name('user.update')->middleware("auth");
Route::post('/user/update-pass', [\App\Http\Controllers\UserController::class, 'updatePassword'])->name('user.update-pass')->middleware("auth");
Route::get('/image/profile/{image}', [\App\Http\Controllers\UserController::class, 'getImage'])->middleware("auth")->name('user.avatar');

Route::get('/upload', [\App\Http\Controllers\ImageController::class, 'uploadView'])->middleware("auth")->name("upload");
Route::post('/upload', [\App\Http\Controllers\ImageController::class, 'savePublication'])->middleware("auth")->name('image.save');
Route::get('/image/publication/{image}', [\App\Http\Controllers\ImageController::class, 'getImage'])->middleware("auth")->name('publication.image');

Route::get('/{perfil}', [\App\Http\Controllers\UserController::class, 'profileView'])->name('profile');