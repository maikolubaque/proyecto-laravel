@extends('layouts.app')

@section('content')

<div class="container">

    @if(session('message'))
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <strong>Mensaje: </strong> {{session('message')}}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif


    <div class="row justify-content-center">
        <div class="col-md-8">

            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                    <a class="nav-link @if($errors->has('password') | $errors->has('old-password')) inactive @else active @endif " id="home-tab" data-bs-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Perfil</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link  @if($errors->has('password') | $errors->has('old-password')) show active @endif" id="profile-tab" data-bs-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Constraseña</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="contact-tab" data-bs-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Otros</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">

                <!-- tab 1: update profile data whith form -->
                <div class="tab-pane fade show @if($errors->has('password') | $errors->has('old-password')) inactive @else active @endif  p-2" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <div class="">
                        <div class="card-body">
                            <div class="mb-2 container-avatar-conf text-center">
                                @include('includes.useravatar')
                            </div>

                            

                            <form method="POST" action="{{route('user.update')}}" enctype="multipart/form-data">
                                @csrf

                                <div class="form-group  row mb-3">
                                    <label for="image" class="col-md-4 col-form-label fw-bolder text-end">Actualizar foto</label>
                                    <div class="col-md-6">
                                        <input type="file" class="form-control-file" name="image">
                                    </div>
                                </div>


                                <div class="form-group row mb-3 ">
                                    <label for="name" class="col-md-4 col-form-label fw-bolder text-end">{{ __('Nombre') }}</label>

                                    <div class="col-md-6">
                                        <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ Auth::user()->name }}" required autocomplete="name" autofocus>

                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row mb-3">
                                    <label for="surname" class="col-md-4 col-form-label fw-bolder text-end">{{ __('Apellido') }}</label>

                                    <div class="col-md-6">
                                        <input id="surname" type="text" class="form-control @error('surname') is-invalid @enderror" name="surname" value="{{ Auth::user()->surname }}" required autocomplete="surname" autofocus>

                                        @error('surname')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row mb-3">
                                    <label for="nick" class="col-md-4 col-form-label fw-bolder text-end">{{ __('nick') }}</label>

                                    <div class="col-md-6">
                                        <input id="nick" type="text" class="form-control @error('nick') is-invalid @enderror" name="nick" value="{{ Auth::user()->nick}}" required autocomplete="nick" autofocus>

                                        @error('nick')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row mb-3">
                                    <label for="email" class="col-md-4 col-form-label fw-bolder text-end">{{ __('E-Mail Address') }}</label>

                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ Auth::user()->email }}" required autocomplete="email">

                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>



                                <div class="form-group row mb-0">
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            {{ __('Guardar Cambios') }}
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>

                <!-- tab 2 -->
                <div class="tab-pane fade p-2 @error('password') show active  @enderror @error('old-password') show active  @enderror" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <form action="{{ route('user.update-pass') }}" method="POST" class="">
                        @csrf
                        <div class="form-group row mb-3">
                            <label for="old-password" class="col-md-5 col-form-label fw-bolder text-end">{{ __('Contraseña Actual') }}</label>

                            <div class="col-md-6">
                                <input id="old-password" type="password" class="form-control @error('old-password') is-invalid @enderror" name="old-password" autocomplete="new-password">

                                @error('old-password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-3">
                            <label for="password" class="col-md-5 col-form-label fw-bolder text-end">{{ __('Nueva Contraseña') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row mb-3">
                            <label for="password-confirm" class="col-md-5 col-form-label fw-bolder text-end">{{ __('Confirmar Contraseña') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Guardar Cambios') }}
                                </button>
                            </div>
                        </div>


                    </form>
                </div>
                <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">...</div>
            </div>


        </div>
    </div>
</div>

@endsection