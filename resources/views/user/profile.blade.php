@extends('layouts.app')

@section('content')
<div class="container-sm">
    <div class="row justify-content-center">
        <div class="col-md-3">

            @include('includes.useravatar')

        </div>
        <div class="col-md-7">
            <strong>Nombre: </strong><span>{{Auth::user()->name}}</span><br>
            <strong>Nombre de usuario: </strong><span>{{Auth::user()->nick}}</span><br>
            <strong>Publicaciones: </strong><span></span>

        </div>
    </div>
    <hr>
    <div class="row">
        @foreach($images as $image)
        <div class="col-sm-6 col-md-4 p-1">
        @if($image->image_path)
                        <img src="{{url('/image/publication/'.$image->image_path)}}" class="img-user-config">
                        @endif
        </div>
        
        @endforeach
    </div>
</div>
@endsection