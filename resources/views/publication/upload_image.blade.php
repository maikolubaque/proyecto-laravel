@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col col-md-8">

            @if(session('message'))
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <strong>Mensaje: </strong> {{session('message')}}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            @endif
            
            <form method="post" action="{{ route('image.save') }}" enctype="multipart/form-data">
                @csrf
                <div class="form-group row mb-3">
                    <label for="image" class="col-md-3 col-form-label fw-bolder text-end">Imagen</label>
                    <div class="col-md-7">
                        <input class="btn btn-primary input-img-upload pb-2 form-control" type="file" name="image" id="image" required>
                        @error('image')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row mb-3">
                    <label for="description" class="col-md-3 col-form-label fw-bolder text-end">{{ __('Descripcion') }}</label>

                    <div class="col-md-7">
                        <textarea id="description" class="form-control @error('description') is-invalid @enderror" name="description" autocomplete="description"></textarea>

                        @error('description')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>

                <div class="row mb-3 form-grup">
                    <div class="col-md-6 offset-md-3">
                        <input type="submit" class="btn btn-primary" value="Enviar">
                    </div>

                </div>
            </form>

        </div>
    </div>
</div>

@endsection