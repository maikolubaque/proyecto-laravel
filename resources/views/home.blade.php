@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @if(session('message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Mensaje: </strong> {{session('message')}}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            @endif

            @foreach($images as $image)
            <div class="card m-2">
                <div class="card-header">
                    <div class="container-avatar d-inline-block align-middle">
                        @if($image->user->image)
                        <img src="{{ url('/image/profile/'.$image->user->image)}}" class="img-user-config">
                        @endif

                    </div>
                    <div class="d-inline align-middle h-100 p-2">
                        <strong>
                            {{$image->user->nick }}
                        </strong>
                    </div>
                </div>
                <div class="card-body p-0">
                    <div class="text-center">
                        @if($image->image_path)
                        <img src="{{url('/image/publication/'.$image->image_path)}}" class="img-user-config">
                        @endif
                    </div>
                    <p class="card-text p-2">
                        {{ $image->description }}
                    </p>

                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection