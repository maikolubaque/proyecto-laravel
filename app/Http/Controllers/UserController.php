<?php

namespace App\Http\Controllers;

use App\Models\Image;
use App\Rules\LastPasswordConfirmation;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File as FacadesFile;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class UserController extends Controller
{
    public $messages = [1=>'Contraseña actualizada',
                        2=> 'Las contraseñas no coinciden',
                        3=> 'La contraseña actual no coincide'];

    public function config()
    {
        return view('user.config');
    }

    public function update(Request $request)
    {
        $id = Auth::user()->id;
        $validate = $this->validate($request, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'surname' => 'required|max:255|unique:users,email,' . $id,
            'nick' => 'required|max:255|unique:users,nick,' . $id,
        ]);

        $id = Auth::user()->id;
        $name = $request->input('name');
        $surname = $request->input('surname');
        $nick = $request->input('nick');
        $email = $request->input('email');
        $image_name = "";

        //subir imagen

        $image = $request->file('image');
        if($image){
            $image_path = time().$image->getClientOriginalName();
            Storage::disk('users')->put($image_path, File::get($image));

            $image_name = $image_path;

            DB::table('users')->where('id', $id)->update([
                'name' => $name,
                'surname' => $surname,
                'nick' => $nick,
                'email' => $email,
                'image' => $image_name,
            ]);
        }else{
            DB::table('users')->where('id', $id)->update([
                'name' => $name,
                'surname' => $surname,
                'nick' => $nick,
                'email' => $email,
            ]);
        }

       

        return redirect()->route('config')->with(['message' => 'Informacion actualizada']);
    }

    public function updatePassword(Request $request)
    {
        $id = Auth::user()->id;

        $validate = $this->validate($request, [
            'password' => ['required', 'string', 'min:6', 'confirmed',],
            'old-password' => [ new LastPasswordConfirmation]
        ]);

        $password = $request->input('password');

        DB::table('users')->where('id', $id)->update([
            'password' => Hash::make($password)
        ]);

        return redirect()->route('config')->with('message','Contraseña actualizada');
    }

    public function getImage($image_path){
        $file = Storage::disk('users')->get($image_path);
        return new Response($file, 200);
    }

    public function profileView($user_nick){
        $images = Image::orderBy('id','desc')->where('user_id', Auth::user()->id)->get();
        return view('user.profile',['images' => $images]);
    }


}
