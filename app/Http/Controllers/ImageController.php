<?php

namespace App\Http\Controllers;

use App\Models\Image;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class ImageController extends Controller
{

    public function uploadView()
    {
        return view("publication.upload_image");
    }

    public function savePublication(Request $request)
    {
        $validate = $this->validate($request, [
            'description' => ['max:255'],
            'image' => ['required'],
        ]);

        $user_id = Auth::user()->id;

        $image = new Image();
        $image->description = $request->input('description');
        $image->image_path = null;
        $image->user_id = $user_id;

        $file = $request->file('image');
        $formatImage = exif_imagetype($file);

        if ($formatImage == IMAGETYPE_JPEG || $formatImage == IMAGETYPE_PNG || $formatImage == IMAGETYPE_GIF) {

            $image_path = time() . $file->getClientOriginalName();
            Storage::disk('images')->put($image_path, File::get($file));

            $image->image_path = $image_path;
            $image->save();

            return redirect()->route('home')->with(['message'=>'Se ha realizado la publicacion']);
        }else{
            return redirect()->route('upload')->with(['message'=>'Formato del archivo invalido']);
        }
    }

    public function getImage($image_path){
        $file = Storage::disk('images')->get($image_path);
        return new Response($file, 200);
    }
}
